import {IStringValidate} from "./index";

export const stringMin = function(this: IStringValidate, value: number, messageError?:string ) {
  if(this.data.length < value){
    this.isValidate = false
    this.messageError+= messageError || `The minimum field length ${value}. `
  }
  return this
}