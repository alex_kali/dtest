import React, {FC} from 'react';
import {
  CheckboxInputStyled,
  CheckboxTextStyled, CustomCheckStyled,
  CustomRadioStyled,
  WrapperCheckboxInputStyled,
  WrapperCheckboxItemStyled
} from "./styled";
import {ErrorTitleStyled} from "../../titles/errorTitle";
import {useField} from "react-redux-hook-form";

interface ICheckboxInput {
  name: string;
  data: Array<{text: string, value: string}>
  required?: boolean;
}

export const CheckboxInput: FC<ICheckboxInput> = (props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
  })
  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  const onChange = (e: any) => {
    if(data){
      if(data.indexOf(e.target.value) === -1){
        changeData([...data,e.target.value])
      }else{
        const newData = (data.filter((i:string) => i !== e.target.value))
        if(newData.length) {
          changeData(newData)
        }else{
          changeData(undefined)
        }
      }
    }else{
      changeData([e.target.value])
    }
  }


  return (
    <WrapperCheckboxInputStyled>
      {props.data.map((i)=>
        <label htmlFor={i.value} key={i.value}>
          <WrapperCheckboxItemStyled checked={data ? data.includes(i.value) : false}>
            <CheckboxTextStyled>{i.text}</CheckboxTextStyled>
            <CheckboxInputStyled id={i.value} type="checkbox" name={props.name} value={i.value} onChange={onChange}/>
            <CustomRadioStyled className={'checkbox'}><CustomCheckStyled /></CustomRadioStyled>
          </WrapperCheckboxItemStyled>
        </label>
      )}
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </WrapperCheckboxInputStyled>
  )
};