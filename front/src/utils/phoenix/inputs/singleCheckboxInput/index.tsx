import React, {FC} from 'react';
import {
  CheckboxInputStyled,
  CheckboxTextStyled, CustomCheckStyled,
  CustomRadioStyled,
  WrapperCheckboxInputStyled,
  WrapperCheckboxItemStyled
} from "./styled";
import {ErrorTitleStyled} from "../../titles/errorTitle";
import {useField} from "react-redux-hook-form";

interface ISingleCheckboxInput {
  name: string;
  text: string;
  required?: boolean;
}

export const SingleCheckboxInput: FC<ISingleCheckboxInput> = (props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
  })
  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()


  const onChange = () => {
    changeData(!data)
  }

  return (
    <WrapperCheckboxInputStyled>
      <label htmlFor={props.name}>
        <WrapperCheckboxItemStyled checked={data}>
          <CheckboxTextStyled>{props.text}</CheckboxTextStyled>
          <CheckboxInputStyled id={props.name} type="checkbox" name={props.name} value={data} onChange={onChange}/>
          <CustomRadioStyled className={'checkbox'}><CustomCheckStyled /></CustomRadioStyled>
        </WrapperCheckboxItemStyled>
      </label>
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </WrapperCheckboxInputStyled>
  )
};