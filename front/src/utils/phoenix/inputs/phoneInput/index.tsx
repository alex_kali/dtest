import React, {FC} from 'react';
import {PhoneInputStyled} from "../phoneInput/styled";
import {ErrorTitleStyled} from "../../titles/errorTitle";
import {validate} from "../../validate";
import {formatter} from "../../formatter";
import {useField} from "react-redux-hook-form";

interface IPhone {
  name: string;
  required?: boolean;
  className?: string;
  isTouch?: boolean;
}

export const PhoneInput: FC<IPhone> = (props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: (value:string) => validate(value + '').string().min(11,'This field is mandatory.'),
    isTouch: props.isTouch,
    isValidate: props.isTouch,
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <PhoneInputStyled
        mask="+7 (999) 999-99-99"
        className={props.className}
        value={data}
        onChange={(e:any)=> {
          if(e.target.value){
            changeData(formatter(e.target.value).string().onlyNumber().data + '' )
          }
        }}
        isValidate={isValidate || !isTouch}
        id={props.name}
        alwaysShowMask
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};
