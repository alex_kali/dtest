import React, { FC } from 'react';
import {PasswordInputStyled} from "./styled";
import {ErrorTitleStyled} from "../../titles/errorTitle";
import {validate} from "../../validate";
import {useField} from "react-redux-hook-form";

interface IPassword {
  name: string;
  required?: boolean;
  isNotValidate?: boolean;
  className?: string;
}

export const PasswordInput: FC<IPassword> = (props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: !props.isNotValidate ? (value:string) => validate(value).string().min(8).max(20) : undefined,
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <PasswordInputStyled
        className={props.className}
        type={'password'}
        value={data || ''}
        onChange={(e)=> {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
        id={props.name}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};
