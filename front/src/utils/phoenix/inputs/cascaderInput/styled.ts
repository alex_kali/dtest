import styled from "styled-components";
import {ReactComponent as plus} from "./icons/plus.svg";
import {ReactComponent as minus} from "./icons/minus.svg";
import {BORDER_INPUT, BORDER_RADIUS_INPUT, COLOR_INPUT, HEIGHT_INPUT, MARGIN_INPUT, PADDING_INPUT} from "../../styles";

export const WrapperSelectStyled = styled.div`
  width: calc(100% - 40px);
  height: ${HEIGHT_INPUT};
  display: flex;
  align-items: center;
  padding-left: ${PADDING_INPUT};
  padding-right: ${PADDING_INPUT};
  margin-top: ${MARGIN_INPUT};
  margin-bottom: ${MARGIN_INPUT};
  z-index: 2;
`

export const TitleStyled = styled.div`
  font-size: 16px;
`

interface IValueStyled {
  isView: boolean;
}

export const ValueStyled = styled.div<IValueStyled>`
  height: 100%;
  border: ${BORDER_INPUT};
  flex-grow: 3;
  margin-left: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  background: white;
  cursor: pointer;
  border-radius:${BORDER_RADIUS_INPUT};
  ${props =>`
    ${props.isView && `
      border-radius: 20px;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
    `}
   `}
`

export const OptionsStyled = styled.div`
  border: ${BORDER_INPUT};
  border-top: none;
  position: absolute;
  width: 100%;
  left: -2px;
  top: 100%;
  background: white;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  overflow: hidden;
  z-index: 2;
`

interface IOptionsItemStyled {
  checked: boolean;
}

export const OptionsItemStyled = styled.div<IOptionsItemStyled>`
  height: 30px;
  display: flex;
  align-items: center;
  border-top: 1px solid ${COLOR_INPUT};
  padding-left: 10px;
  &:hover{
    background: rgba(0,0,0,0.1);
  }
  ${props => props.checked ? 'background: rgba(0,0,0,0.1);' : ''}
`

export const PlusStyled = styled(plus)`
  width: 14px;
  height: 14px;
  padding: 3px;
  margin-right: 10px;
  margin-left: -10px;
  position: relative;
  &::before {
    content: '';
    position: absolute;
    left: -5px;
    top: -5px;
    width: calc(100% + 10px);
    height: calc(100% + 10px);
  }
`

export const MinusStyled = styled(minus)`
  margin-right: 10px;
  margin-left: -10px;
  width: 14px;
  height: 14px;
  padding: 3px;
  position: relative;
  rect{
    fill: black;
  }
  &::before {
    content: '';
    position: absolute;
    left: -5px;
    top: -5px;
    width: calc(100% + 10px);
    height: calc(100% + 10px);
  }
`