import styled from "styled-components";
import {
  BORDER_INPUT,
  BORDER_RADIUS_INPUT, COLOR_INPUT,
  ERROR_BORDER_COLOR_INPUT,
  ERROR_COLOR_INPUT, FONT_SIZE_INPUT, HEIGHT_INPUT, MARGIN_INPUT, OPACITY_FOCUS_INPUT,
  OPACITY_INPUT,
  PADDING_INPUT,
  WIDTH_INPUT
} from "../../styles";

interface IPasswordInputStyled {
  isValidate: boolean;
}

export const EmailInputStyled = styled.input<IPasswordInputStyled>`
  background: none;
  margin-top: ${MARGIN_INPUT};
  margin-bottom: ${MARGIN_INPUT};
  width: ${WIDTH_INPUT};
  border: ${BORDER_INPUT};
  border-radius: ${BORDER_RADIUS_INPUT};
  height: ${HEIGHT_INPUT};
  font-size: ${FONT_SIZE_INPUT};
  padding-left: ${PADDING_INPUT};
  padding-right: ${PADDING_INPUT};
  color: ${COLOR_INPUT};
  opacity: ${OPACITY_INPUT};
  &:focus{
    outline: none;
    opacity: ${OPACITY_FOCUS_INPUT};
  }
  ${props => `
    color: ${props.isValidate ? '' : ERROR_COLOR_INPUT} !important;
    border-color: ${props.isValidate ? '' : ERROR_BORDER_COLOR_INPUT} !important;
    margin-bottom: ${props.isValidate ? '' : '4px'} !important;
    &:focus{
      color: ${props.isValidate ? '' : ERROR_COLOR_INPUT} !important;
      border-color: ${props.isValidate ? '' : ERROR_BORDER_COLOR_INPUT} !important;
    }
  `}
`