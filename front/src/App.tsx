import React from 'react';
import './App.css';
import {Provider} from "react-redux";
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import {Footer} from "src/components/footer";
import {Header} from "src/components/header";
import {BasketPage} from "src/pages/basket";
import {AuthForm} from "src/pages/login";
import {ProductPage} from "src/pages/product";
import {ProductsPage} from "src/pages/products";
import {store} from "src/store/store";
import {AppStyled} from "src/styled";

function App() {
  return (
    <AppStyled>
      <Router>
        <Provider store={store}>
          <Header/>
          <Switch>
            <Route path={`/product/:id`}>
              <ProductPage />
            </Route>
            <Route path={`/products`}>
              <ProductsPage />
            </Route>
            <Route path={`/login`}>
              <AuthForm/>
            </Route>
            <Route path={`/registration`}>
              <AuthForm/>
            </Route>
            <Route path={`/basket`}>
              <BasketPage/>
            </Route>
          </Switch>
          <Footer/>
        </Provider>
      </Router>
    </AppStyled>
  );
}

export default App;
