import {FC} from "react";
import {AddButton, DeleteButton, LeftBlock, ProductStyled, RightBlock} from "src/components/ui/product/styled";
import {ImageStyled, PriceStyled, TitleStyled} from "src/components/ui/product/styled";
import {User} from "src/store/auth/model";
import {IProduct} from "src/store/product/model";
import {useNavigation} from "src/utils/useNavigation";

export const ProductDetail:FC<IProduct> = (props) => {
  const navigate = useNavigation()
  const basket = User.basket.useData()
  let isInBasket = false
  
  if(basket){
    for (let i of basket){
      if(i.id === props.id){
        isInBasket = true;
        break
      }
    }
  }
  
  const onDelete = (event:React.MouseEvent<HTMLElement>) => {
    event.stopPropagation()
    User.basket.delete(props)
  }
  
  const onAdd = (event:React.MouseEvent<HTMLElement>) => {
    event.stopPropagation()
    User.basket.add(props)
  }
  
  return (
    <ProductStyled onClick={() => navigate(`/product/${props.id}`)}>
      <LeftBlock>
        <ImageStyled src={process.env.REACT_APP_SERVER_API_URL + props.image}/>
      </LeftBlock>
      <RightBlock>
        <TitleStyled>{props.title}</TitleStyled>
        <PriceStyled>{props.price}</PriceStyled>
        {isInBasket && <DeleteButton onClick={onDelete}>Удалить из корзины</DeleteButton>}
        {!isInBasket && <AddButton onClick={onAdd}>Добавить в корзину</AddButton>}
      </RightBlock>
    </ProductStyled>
  )
}