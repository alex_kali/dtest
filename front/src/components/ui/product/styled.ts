import styled from "styled-components";

export const ProductStyled = styled.div`
  margin-top: 10px;
  margin-bottom: 10px;
  border: 1px solid black;
  padding: 10px;
  display: flex;
  cursor: pointer;
`

export const ImageStyled = styled.img`
  width: 250px;
  height: 200px;
  object-fit: cover;
`

export const TitleStyled = styled.div`
  text-align: center;
`

export const PriceStyled = styled.div`
  text-align: center;
`

export const LeftBlock = styled.div`
  flex: 0 0 250px;
`

export const RightBlock = styled.div`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
`

export const DeleteButton = styled.div`
  border: 1px solid #7c4343;
  color: #7c4343;
  padding: 4px;
  margin: auto;
  margin-bottom: 10px;
  width: fit-content;
`

export const AddButton = styled.div`
  border: 1px solid green;
  color: green;
  padding: 4px;
  margin: auto;
  margin-bottom: 10px;
  width: fit-content;
`