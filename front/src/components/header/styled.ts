import styled from "styled-components";

export const HeaderStyled = styled.header`
  width: 100%;
  height: 60px;
  background: grey;
  
  display: flex;
  align-items: center;
  gap: 10px;
  padding-left: 10px;
  padding-right: 10px;
  box-sizing: border-box;
`

export const LinkStyled = styled.div`
  padding: 4px;
  border: 1px solid black;
  cursor: pointer;
`
