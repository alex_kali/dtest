import {HeaderStyled, LinkStyled} from "src/components/header/styled";
import {User} from "src/store/auth/model";
import {useAccess} from "src/store/auth/useAccess";
import {useToken} from "src/store/auth/useToken";
import {useNavigation} from "src/utils/useNavigation";

export const Header = () => {
  const [userToken, setUserToken] = useToken()
  const userData = User.useData()
  const navigate = useNavigation()

  return (
    <HeaderStyled>
      {userToken && userData && (
        <>
          <LinkStyled onClick={() => navigate('/basket')}>username: {userData.username}</LinkStyled>
          <LinkStyled onClick={() => navigate('/products')}>Список продуктов</LinkStyled>
          <LinkStyled onClick={() => {
            navigate('/login')
            User.options.logout()
            setUserToken('')
          }}>Logout</LinkStyled>
        </>
      )}
    </HeaderStyled>
  )
}