import {configureStore, ThunkAction, Action, combineReducers} from '@reduxjs/toolkit';
import { formControllerReducer } from 'react-redux-hook-form';
import {requestControllerReducer} from "react-redux-request-controller";
import productReducer from './product/slices';
import productsReducer from './products/slices';
import userReducer from './auth/slices';

const combinedReducer = combineReducers({
  formController: formControllerReducer, // подключение formControllerReducer
  requestController: requestControllerReducer, // подключение requestControllerReducer
  product: productReducer,
  products: productsReducer,
  user: userReducer,
});

const rootReducer = (state:any, action:any) => {
  if (action.type === 'user/logoutUser') {
    for(let i in state){
      if(i !== 'formController'){
        state[i] = undefined
      }
    }
  }
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export const dispatch = store.dispatch
export const getState = store.getState

// @ts-ignore
window.dispatch = store.dispatch

// @ts-ignore
window.getState = store.getState

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;