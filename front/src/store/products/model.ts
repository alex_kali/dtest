import {useEffect} from "react";
import {useSelector} from "react-redux";
import {IProduct} from "src/store/product/model";
import {getProducts} from "src/store/products/thunks";
import {dispatch, getState, RootState} from "src/store/store";

export const Products = {
  
  useData: function() {
    const data = useSelector((state: RootState) => state.products.products) as null | Array<IProduct>
    
    useEffect(() => {
      if(!data && !this.getIsLoading()){
        dispatch(getProducts())
      }
    }, [data])
    return data
  },
  
  
  getIsLoading: function (){
    return getState().requestController.isLoading.includes(`getProducts`) as boolean
  }
}

