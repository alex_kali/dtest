import {requestController} from "react-redux-request-controller";
import {setProducts} from "src/store/products/slices";
import {AppThunk} from "src/store/store";



export const getProducts = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getProducts`,
    url: `/api/products/?populate=image`,
    method: 'GET',
    action: setProducts,
    serialize: (data) => {
      for(let i in data.data){
        const id = data.data[i].id
        data.data[i] = data.data[i].attributes
        data.data[i].id = id
        data.data[i].image = data.data[i].image.data.attributes.url
      }
      return data.data
    },
  }))
};