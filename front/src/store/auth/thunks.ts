import {requestController} from "react-redux-request-controller";
import {logoutUser, setBasket, setUser} from "src/store/auth/slices";
import {AppThunk} from "src/store/store";

export const login = (data:any): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: 'login',
    params: data,
    url: '/api/auth/local',
    method: 'POST',
    action: setUser
  }))
};

export const registration = (data:any): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: 'login',
    params: data,
    url: '/api/auth/local/register',
    method: 'POST',
    action: setUser
  }))
};

export const getUserData = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: 'getUserData',
    url: '/api/users/me',
    method: 'GET',
    action: setUser
  }))
};

export const logout = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(logoutUser())
};

export const getBasketItems = (id: string | number): AppThunk => async (
  dispatch: any
) => {
  
  dispatch(requestController({
    id: `getBasketItems`,
    url: `/api/products/?filters[user_products][user][id][$contains]=${id}&populate=image`,
    method: 'GET',
    action: setBasket,
    serialize: (data) => {
      for(let i in data.data){
        const id = data.data[i].id
        data.data[i] = data.data[i].attributes
        data.data[i].id = id
        data.data[i].image = data.data[i].image.data.attributes.url
      }
      return data.data
    },
  }))
};

export const addBasketItem = (id: string | number, userId: string | number): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `addBasketItem`,
    url: `/api/user-products`,
    method: 'POST',
    params: {data: {product: id, user: userId}},
    action: null,
  }))
};

export const deleteBasketItem = (basketId: string | number, userId: string | number): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getBasketUserItem`,
    url: `/api/user-products/?filters[user][id][$contains]=${userId}&filters[product][id][$contains]=${basketId}`,
    method: 'GET',
    onSuccess: (data) => {
      for (let i of data.data) {
        dispatch(requestController({
          id: `deleteBasketItem`,
          url: `/api/user-products/${i.id}`,
          method: 'DELETE',
          action: null,
        }))
      }
    },
    action: null,
  }))
};