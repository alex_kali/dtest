import {useSelector} from "react-redux";
import useCookie from 'react-use-cookie';
import {useEffect} from "react";
import {useNavigation} from "src/utils/useNavigation";
import {User} from "./model";
import { useBeforeRender } from "react-redux-hook-form";

declare global {
  interface Window {
    token: string | undefined;
  }
}

export const useToken = () => {
  const [userToken, setUserToken] = useCookie('token');
  const token = useSelector((state: any) => state.user.user?.token)
  const navigate = useNavigation()

  useBeforeRender(()=>{
    if(userToken){
      window.token = userToken
      User.options.getData()
    } else if (!userToken) {
      navigate('/login')
    }
  })
  
  useEffect(() => {
    if(userToken) {
      if(window.location.pathname === '/login' || window.location.pathname === '/registration') {
        navigate('/products')
      }
    }
  }, [userToken])

  useEffect(()=>{
    if(token){
      setUserToken('bearer ' + token)
      window.token = 'bearer ' + token
    }
  }, [token, setUserToken])

  return [userToken, setUserToken as any]
}