import {useEffect} from "react";
import {setBasket} from "src/store/auth/slices";
import {IProduct} from "src/store/product/model";
import {dispatch, getState, RootState, store} from "../store";
import {addBasketItem, deleteBasketItem, getBasketItems, getUserData, login, logout, registration} from "./thunks";
import {useSelector} from "react-redux";
import {useIsError, useIsLoading} from "react-redux-request-controller";

export const User = {
  useData: () => useSelector((state: RootState) => state.user.user) as IUser | null,
  useIsLoading: () => useIsLoading('getUserData'),
  useIsError: () => useIsError('getUserData'),
  
  basket: {
    useData: function() {
      const userId = useSelector((state: RootState) => state.user.user?.id) as string| number | null;
      const data = useSelector((state: RootState) => state.user.basket) as null | Array<IProduct>
  
      useEffect(() => {
        const isLoading = getState().requestController.isLoading.includes(`getBasketItems`) as boolean
        if(!data && !isLoading && userId){
          dispatch(getBasketItems(userId))
        }
      }, [data, userId])
  
      return data
    },
    delete: function(item: IProduct) {
      dispatch(setBasket((this.get() as Array<IProduct>).filter((i) => i.id !== item.id)))
      dispatch(deleteBasketItem(item.id, store.getState().user.user.id))
    },
    add: function(item: IProduct) {
      dispatch(setBasket([...[item], ...(this.get() as Array<IProduct>)]))
      dispatch(addBasketItem(item.id, store.getState().user.user.id))
    },
    get: () => {
      return store.getState().user.basket
    }
  },
  
  object: {
    getData: () => store.getState().user.user as IUser | null,
  },
  options: {
    login: (data:ILogin) => dispatch(login(data)),
    registration: (data:IRegistration) => dispatch(registration(data)),
    getData: () => dispatch(getUserData()),
    logout: () => dispatch(logout()),
  },
}

export interface IUser {
  id: string | number;
  username?: string;
  email?: string;
  token?: string;
}

//****************** Options interface ******************//

export interface ILogin {
  username: string;
  password: string;
}

export interface IRegistration {
  username: string;
  email: string;
  password: string;
}
