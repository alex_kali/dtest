import {createSlice} from "@reduxjs/toolkit";

export const initialState: any = {
  user: null,
  basket: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      if(action.payload.jwt){
        state.user = action.payload.user
        state.user.token = action.payload.jwt
      } else {
        state.user = action.payload
      }
    },
    logoutUser: state => {
    
    },
    setBasket: (state, action) => {
      state.basket = action.payload
    },
  },
});

export const { setUser, logoutUser, setBasket } = userSlice.actions

export default userSlice.reducer;