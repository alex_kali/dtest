import {requestController} from "react-redux-request-controller";
import {setProduct} from "src/store/product/slices";
import {AppThunk} from "src/store/store";


export const getProduct = (id: number | string): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getProduct`,
    url: `/api/products/${id}?populate=image`,
    method: 'GET',
    action: setProduct,
    serialize: (data) => {
      const id = data.id
      data = data.data.attributes
      data.id = id
      data.image = data.image.data.attributes.url
      return data
    }
  }))
};