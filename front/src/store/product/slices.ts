import {createSlice} from "@reduxjs/toolkit";
export const initialState: any = {
  product: null,
};

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    setProduct: (state, action) => {
      state.product = action.payload
    },
  },
});

export const { setProduct } = productSlice.actions

export default productSlice.reducer;