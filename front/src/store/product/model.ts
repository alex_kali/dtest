import {useEffect} from "react";
import {useSelector} from "react-redux";
import {setProduct} from "src/store/product/slices";
import {getProduct} from "src/store/product/thunks";
import {dispatch, getState, RootState} from "src/store/store";


export const Product = {
  id: null as null | number | string,
  
  useData: function(id: number | string) {
    this.id = id
    const data = useSelector((state: RootState) => state.product.product) as IProduct | null
    
    useEffect(() => {
      if(data !== null){
        dispatch(setProduct(null))
      }
    }, [id])
    
    useEffect(() => {
      if(!data && !this.getIsLoading()){
        dispatch(getProduct(id))
      }
    }, [data])
    
    return data
  },
  
  getIsLoading: function (){
    return getState().requestController.isLoading.includes(`getRecipeDetail${this.id}`) as boolean
  }
}

export interface IProduct {
  id: string | number;
  title: string;
  image: string;
  price: number;
}
