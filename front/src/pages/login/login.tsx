import {FC} from "react";
import {useForm, WrapperForm} from "react-redux-hook-form";
import {WrapperLoginStyled} from "src/pages/login/styled";
import {ILogin, User} from "src/store/auth/model";
import {SubmitButton} from "src/utils/phoenix/buttons";
import {PasswordInput} from "src/utils/phoenix/inputs/password";
import {PhoneInput} from "src/utils/phoenix/inputs/phoneInput";
import {InputTitleStyled} from "src/utils/phoenix/titles/inputTitle";


export const Login:FC = () => {
  const form = useForm({name: 'login'})
  return (
    <WrapperLoginStyled>
      <WrapperForm form={form} onSubmit={(data:ILogin) => {User.options.login(data)}}>
        <InputTitleStyled>Phone number</InputTitleStyled>
        <PhoneInput name={'identifier'} required/>
  
        <InputTitleStyled>Password</InputTitleStyled>
        <PasswordInput name={'password'} required/>
  
        <SubmitButton text={'Authorize'}/>
      </WrapperForm>
    </WrapperLoginStyled>
  )
}