import React, {FC} from "react";
import {useNavigation} from "src/utils/useNavigation";
import {
  AuthFormStyled,
  TitleStyled,
  WrapperFormStyled,
  WrapperTitleStyled
} from "./styled";
import { useParams} from "react-router-dom";
import {Registration} from "./registration";
import {Login} from "./login";

export const AuthForm:FC = () => {
  const navigate = useNavigation()
  
  return (
    <AuthFormStyled>
      <WrapperFormStyled>
        <WrapperTitleStyled>
          <TitleStyled isActive={window.location.pathname === '/login'} onClick={()=>navigate('/login')}>Authorization</TitleStyled>
          <TitleStyled isActive={window.location.pathname === '/registration'} onClick={()=>navigate('/registration')} style={{borderLeft: '2px solid #36393E'}}>Registration</TitleStyled>
        </WrapperTitleStyled>
  
        {window.location.pathname !== '/registration' && <Login/>}
        {window.location.pathname === '/registration' && <Registration/>}
        
      </WrapperFormStyled>
    </AuthFormStyled>
  )
}