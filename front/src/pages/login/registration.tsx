import {FC, useEffect} from "react";
import {useForm, WrapperForm} from "react-redux-hook-form";
import {WrapperRegistrationStyled} from "src/pages/login/styled";
import {IRegistration, User} from "src/store/auth/model";
import {SubmitButton} from "src/utils/phoenix/buttons";
import {EmailInput} from "src/utils/phoenix/inputs/emailInput";
import {PasswordInput} from "src/utils/phoenix/inputs/password";
import {PhoneInput} from "src/utils/phoenix/inputs/phoneInput";
import {InputTitleStyled} from "src/utils/phoenix/titles/inputTitle";

export const Registration:FC = () => {
  const form = useForm({name: 'registration'})

  const password1 = form.useFieldSelector('password')
  const password2 = form.useFieldSelector('password2')

  useEffect(()=>{
    if(password1 !== password2){
      form.changeIsValidateField('password2',false)
      form.changeMessageErrorField('password2', 'Fields must match')
    }
  },[password1, password2, form])

  return (
    <WrapperRegistrationStyled>
      <WrapperForm form={form} onSubmit={(data:IRegistration) => {User.options.registration(data)}}>
        <InputTitleStyled>Phone number</InputTitleStyled>
        <PhoneInput name={'username'} required/>
  
        <InputTitleStyled>Email</InputTitleStyled>
        <EmailInput name={'email'} required/>
  
        <InputTitleStyled>Password</InputTitleStyled>
        <PasswordInput name={'password'} required/>
  
        <InputTitleStyled>Repeat password</InputTitleStyled>
        <PasswordInput name={'password2'} required isNotValidate/>
  
        <SubmitButton text={'Register'}/>
      </WrapperForm>
    </WrapperRegistrationStyled>
  )
}