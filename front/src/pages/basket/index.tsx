import {ProductDetail} from "src/components/ui/product";
import {BasketStyled} from "src/pages/basket/styled";
import {User} from "src/store/auth/model";

export const BasketPage = () => {
  const basket = User.basket.useData()

  if(!basket){
    return null
  }
  
  return (
    <BasketStyled>
      {basket.map((item) => <ProductDetail {...item}/>)}
    </BasketStyled>
  )
}