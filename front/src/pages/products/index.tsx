import {ProductDetail} from "src/components/ui/product";
import {ProductsStyled} from "src/pages/products/styled";
import {User} from "src/store/auth/model";
import {Products} from "src/store/products/model";

export const ProductsPage = () => {
  const products = Products.useData()
  
  if(!products){
    return null
  }
  
  return (
    <ProductsStyled>
      {products.map((item) => <ProductDetail {...item}/>)}
    </ProductsStyled>
  )
}