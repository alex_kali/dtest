import styled from "styled-components";

export const ProductStyled = styled.div`
  margin-left: auto;
  margin-right: auto;
  width: 300px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 10px;
`

export const ImageStyled = styled.img`
  width: 250px;
  height: 200px;
  object-fit: cover;
`

export const TitleStyled = styled.div`
  text-align: center;
`

export const PriceStyled = styled.div`
  text-align: center;
`