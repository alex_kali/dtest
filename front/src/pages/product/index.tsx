import {useParams} from "react-router-dom";
import {ImageStyled, PriceStyled, ProductStyled, TitleStyled} from "src/pages/product/styled";
import {User} from "src/store/auth/model";
import {Product} from "src/store/product/model";

export const ProductPage = () => {
  const { id }:{id: string} = useParams()
  const product = Product.useData(id)
  
  if(!product){
    return null
  }
  
  return (
    <ProductStyled>
      <TitleStyled>{product.title}</TitleStyled>
      <PriceStyled>{product.price}</PriceStyled>
      <ImageStyled src={process.env.REACT_APP_SERVER_API_URL + product.image}/>
    </ProductStyled>
  )
}